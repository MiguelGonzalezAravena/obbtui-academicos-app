import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';

// Páginas
import { AccesoPage } from '../pages/acceso/acceso';
import { AsignaturaCorreoPage } from '../pages/asignatura-correo/asignatura-correo';
import { AsignaturaNotificacionPage } from '../pages/asignatura-notificacion/asignatura-notificacion';
import { AsignaturasPage } from '../pages/asignaturas/asignaturas';
import { CasinosPage } from '../pages/casinos/casinos';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { CredencialPage } from '../pages/credencial/credencial';
import { DeudasPage } from '../pages/deudas/deudas';
import { EnlacesPage } from '../pages/enlaces/enlaces';
import { EventosInfoPage } from '../pages/eventos-info/eventos-info';
import { EventosPage } from '../pages/eventos/eventos';
import { HorarioPage } from '../pages/horario/horario';
import { InformacionPage } from '../pages/informacion/informacion';
import { MenuAsignaturaPage } from '../pages/menu-asignatura/menu-asignatura';
import { NotificacionesPage } from '../pages/notificaciones/notificaciones';
import { NovedadesPage } from '../pages/novedades/novedades';
import { PerfilPage } from '../pages/perfil/perfil';
import { PresentacionPage } from '../pages/presentacion/presentacion';
import { PuntosPage } from '../pages/puntos/puntos';
import { PuntosMapaPage } from '../pages/puntos-mapa/puntos-mapa';
import { SalirPage } from '../pages/configuracion/salir';
import { SantanderPage } from '../pages/configuracion/santander';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';

// Componentes
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';

// Modulos
import { QRCodeModule } from 'angular2-qrcode';

// Proveedores
import { AccesoProvider } from '../providers/acceso/acceso';
import { AsignaturasProvider } from '../providers/asignaturas/asignaturas';
import { AulaVirtualProvider } from '../providers/aula-virtual/aula-virtual';
import { CasinosProvider } from '../providers/casinos/casinos';
import { DeudasProvider } from '../providers/deudas/deudas';
import { EnlacesProvider } from '../providers/enlaces/enlaces';
import { EventosProvider } from '../providers/eventos/eventos';
import { HorarioProvider } from '../providers/horario/horario';
import { NotificacionesProvider } from '../providers/notificaciones/notificaciones';
import { NovedadesProvider } from '../providers/novedades/novedades';
import { PeriodoProvider } from '../providers/periodo/periodo';
import { PortalAcademicoProvider } from '../providers/portal-academico/portal-academico';
import { PuntosProvider } from '../providers/puntos/puntos';
import { SesionProvider } from '../providers/sesion/sesion';
import { UxProvider } from '../providers/ux/ux';

// Plugins
import { AppAvailability } from '@ionic-native/app-availability';
import { AppVersion } from '@ionic-native/app-version';
import { BackgroundMode } from '@ionic-native/background-mode';
import { CallNumber } from '@ionic-native/call-number';
import { Dialogs } from '@ionic-native/dialogs';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Market } from '@ionic-native/market';
import { NativeStorage } from '@ionic-native/native-storage';
import { OneSignal } from '@ionic-native/onesignal';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

@NgModule({
  declarations: [
    MyApp,
    AccesoPage,
    AsignaturaCorreoPage,
    AsignaturaNotificacionPage,
    CasinosPage,
    PresentacionPage,
    NovedadesPage,
    PerfilPage,
    NotificacionesPage,
    TabsNavigationPage,
    ConfiguracionPage,
    SalirPage,
    CredencialPage,
    EventosPage,
    EventosInfoPage,
    AsignaturasPage,
    HorarioPage,
    EnlacesPage,
    PuntosPage,
    PuntosMapaPage,
    InformacionPage,
    DeudasPage,
    SantanderPage,
    MenuAsignaturaPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ComponentsModule,
    PipesModule,
    QRCodeModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Atrás'
    })
  ],
  bootstrap: [ IonicApp ],
  entryComponents: [
    MyApp,
    AccesoPage,
    AsignaturaCorreoPage,
    AsignaturaNotificacionPage,
    CasinosPage,
    PresentacionPage,
    NovedadesPage,
    PerfilPage,
    NotificacionesPage,
    TabsNavigationPage,
    ConfiguracionPage,
    SalirPage,
    CredencialPage,
    EventosPage,
    EventosInfoPage,
    AsignaturasPage,
    HorarioPage,
    EnlacesPage,
    PuntosPage,
    PuntosMapaPage,
    InformacionPage,
    DeudasPage,
    SantanderPage,
    MenuAsignaturaPage
  ],
  providers: [
    AppAvailability,
    AppVersion,
    BackgroundMode,
    CallNumber,
    Dialogs,
    Geolocation,
    GoogleAnalytics,
    InAppBrowser,
    LaunchNavigator,
    Market,
    NativeStorage,
    StatusBar,
    SplashScreen,
    OneSignal,

    AccesoProvider,
    AsignaturasProvider,
    AulaVirtualProvider,
    CasinosProvider,
    DeudasProvider,
    EnlacesProvider,
    EventosProvider,
    HorarioProvider,
    NotificacionesProvider,
    NovedadesProvider,
    PortalAcademicoProvider,
    PuntosProvider,
    PeriodoProvider,
    SesionProvider,
    UxProvider

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}