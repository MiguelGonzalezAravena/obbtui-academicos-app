import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CredencialComponent } from './credencial/credencial';
import { PreloadImage } from './preload-image/preload-image';
import { ShowHideContainer } from './show-hide-password/show-hide-container';
import { ShowHideInput } from './show-hide-password/show-hide-input';
import { QRCodeModule } from 'angular2-qrcode';
import { SinRegistrosComponent } from './sin-registros/sin-registros';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

@NgModule({
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    PreloadImage,
    ShowHideContainer,
    ShowHideInput,
    CredencialComponent,
    SinRegistrosComponent
  ],
  imports: [
    QRCodeModule,
    CommonModule,
    IonicModule
  ],
  exports: [
    ShowHideContainer,
    ShowHideInput,
    PreloadImage,
    CredencialComponent,
    SinRegistrosComponent
  ]
})
export class ComponentsModule {}