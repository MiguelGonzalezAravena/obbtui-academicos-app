import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { AsignaturasProvider } from '../../providers/asignaturas/asignaturas';
import { PortalAcademicoProvider } from '../../providers/portal-academico/portal-academico';
import { UxProvider } from '../../providers/ux/ux';
import { MenuAsignaturaPage } from '../menu-asignatura/menu-asignatura';
import { GlobalVar } from '../../config';
import {NativeStorage} from '@ionic-native/native-storage';

@Component({
  selector: 'page-asignaturas',
  templateUrl: 'asignaturas.html',
})
export class AsignaturasPage {
  asignaturas: any;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private navCtrl: NavController,
    private asignaturasProvider: AsignaturasProvider,
    private portalAcademicoProvider: PortalAcademicoProvider,
    private ux: UxProvider
  ) {
    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.asignaturasProvider.getAsignaturas(token)
          .subscribe(dataAsignaturas => {
            this.ux.hideCargando();
            console.log(dataAsignaturas);
            if(dataAsignaturas.status == 'ERROR') {
              this.ux.alerta(dataAsignaturas.error.message);
            } else if(dataAsignaturas.status == 'OK') {
              this.asignaturas = dataAsignaturas.data;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  ionViewDidLoad() {
    this.ga.trackView('page-asignaturas');
  }

  verMenuAsignatura(asignatura) {
    this.navCtrl.push(MenuAsignaturaPage, { asignatura: asignatura });
  }

  portalAcademico() {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.portalAcademicoProvider.getUrlPortal(token)
          .subscribe(dataPortal => {
            this.ux.hideCargando();
            if(dataPortal.status == 'ERROR') {
              this.ux.alerta(dataPortal.error.message);
            } else if(dataPortal.status == 'OK') {
              window.open(dataPortal.data.url, '_system');
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }
}