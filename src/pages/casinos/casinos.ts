import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { UxProvider } from '../../providers/ux/ux';
import { CasinosProvider } from '../../providers/casinos/casinos';
import { PuntosMapaPage } from '../puntos-mapa/puntos-mapa';
import { GlobalVar } from '../../config';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'page-casinos',
  templateUrl: 'casinos.html',
})
export class CasinosPage {
  casinos: any;
  casino: any;
  casino_datos: any;
  menus: any;
  fecha_texto: any;
  fecha: any;
  segmento: string;
  str_fecha: any;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private navCtrl: NavController,
    private casinosProvider: CasinosProvider,
    private ux: UxProvider
  ) {
    let fecha = new Date();
    this.fecha = fecha;
    this.fecha_texto = this.strDate(fecha.getUTCDate()) + ' de ' + this.strMonth(fecha.getUTCMonth()) + ' de ' + fecha.getUTCFullYear();
    this.str_fecha = fecha.getUTCFullYear() + this.strDate(fecha.getUTCMonth() + 1) + this.strDate(fecha.getUTCDate());
    this.segmento = 'hoy';

    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    this.nativeStorage.getItem('token')
      .then(token => {
        // Obtener lista de casinos
        this.ux.showCargando();
        this.casinosProvider.getCasinos(token)
          .subscribe(dataCasinos => {
            this.ux.hideCargando();
            console.log(dataCasinos);
            if(dataCasinos.status == 'ERROR') {
              this.ux.alerta(dataCasinos.error.message);
            } else if(dataCasinos.status == 'OK') {
              this.casinos = dataCasinos.data;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  ionViewDidLoad() {
    this.ga.trackView('page-casinos');
  }

  getClass(menu) {
    switch(menu) {
      case 'HIPOCALÓRICO':
        return 'categoria-hipocalorico';
      case 'EMBARAZADA':
        return 'categoria-embarazada';
      case 'VEGETARIANO':
        return 'categoria-vegetariano';
      default:
      case 'GENERAL':
        return 'categoria-general';
    }
  }

  cambiarDia(segmento) {
    let f = new Date();
    let d = f.getUTCDay();
    switch(segmento.value) {
      case 'hoy':
        if(d == 6 || d == 0) {
          // Aumentar 1 ó 2 días el fin de semana
          f.setDate( f.getUTCDate() + (d == 6 ? 2 : 1) );
        }
        break;
      case 'manana':
        if(d >= 1 && d <= 4) {
          // Aumentar 1 día
          f.setDate( f.getUTCDate() + 1 );
        } else if(d == 5) {
          // Aumentar 3 días el viernes
          f.setDate( f.getUTCDate() + 3 );
        } else {
          // Aumentar 2 ó 3 días
          f.setDate( f.getUTCDate() + (d == 6 ? 3 : 2) );
        }
        break;
    }

    // Cambiar fecha
    this.str_fecha = f.getUTCFullYear() + this.strDate(f.getUTCMonth() + 1) + this.strDate(f.getUTCDate());
    this.fecha_texto = this.strDate(f.getUTCDate()) + ' de ' + this.strMonth(f.getUTCMonth()) + ' de ' + f.getUTCFullYear();
    this.menus = null;
    this.getMenus(this.casino);
    console.log('fechaCambiada:', this.str_fecha);
  }

  getMenus(casino) {
    this.nativeStorage.getItem('token')
      .then(token => {
        this.casino = casino;
        this.casino_datos = this.casinos.filter((data) => { return data.cod_casino == casino });

        // Obtener menús según casino
        this.ux.showCargando();
        this.casinosProvider.getMenus(token, casino, this.str_fecha)
          .subscribe((dataMenus) => {
            this.ux.hideCargando();
            console.log(dataMenus);
            if(dataMenus.status == 'ERROR') {
              this.ux.alerta(dataMenus.error.message);
            } else if(dataMenus.status == 'OK') {
              this.menus = dataMenus.data;
            }
          }, (err) => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  strMonth(month) {
    switch(month) {
      case 0:
        return 'Enero';
      case 1:
        return 'Febrero';
      case 2:
        return 'Marzo';
      case 3:
        return 'Abril';
      case 4:
        return 'Mayo';
      case 5:
        return 'Junio';
      case 6:
        return 'Julio';
      case 7:
        return 'Agosto';
      case 8:
        return 'Septiembre';
      case 9:
        return 'Octubre';
      case 10:
        return 'Noviembre';
      case 11:
        return 'Diciembre';
    }
  }

  strDate(number) {
    return ('0' + number).substr(-2);
  }

  ubicacionCasino() {
    console.log('ubicacionCasino:', this.casino_datos[0]);
    this.navCtrl.push(PuntosMapaPage, { punto: this.casino_datos[0] });
  }
}