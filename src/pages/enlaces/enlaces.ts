import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { EnlacesProvider } from '../../providers/enlaces/enlaces';
import { UxProvider } from '../../providers/ux/ux';
import { GlobalVar } from '../../config';

@Component({
  selector: 'page-enlaces',
  templateUrl: 'enlaces.html',
})
export class EnlacesPage {
  enlaces: any;
  enlacesFiltrados: any;
  str_enlace: string;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private enlacesProvider: EnlacesProvider,
    private ux: UxProvider
  ) {
    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.enlacesProvider.getEnlaces(token)
          .subscribe(dataEnlaces => {
            this.ux.hideCargando();
            console.log(dataEnlaces);
            if(dataEnlaces.status == 'ERROR') {
              this.ux.alerta(dataEnlaces.error.message);
            } else if(dataEnlaces.status == 'OK') {
              this.enlaces = dataEnlaces.data;
              this.enlacesFiltrados = this.enlaces;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  ionViewDidLoad() {
    this.ga.trackView('page-enlaces');
  }

  buscarEnlace(item) {
    this.enlacesFiltrados = this.enlaces.filter(elem => {
      return elem.nombre.toLowerCase().search(this.str_enlace.toLowerCase()) != -1;
    });
  }

  abrirEnlace(enlace) {
    window.open(enlace, '_system');
  }

  cancelarBusqueda(item) {
    this.enlacesFiltrados = this.enlaces;
  }
}