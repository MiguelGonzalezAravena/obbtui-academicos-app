import { Component } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { NativeStorage } from '@ionic-native/native-storage';
import { NavController } from 'ionic-angular';
import { EventosProvider } from '../../providers/eventos/eventos';
import { UxProvider } from '../../providers/ux/ux';
import { EventosInfoPage } from '../eventos-info/eventos-info';
import { GlobalVar } from '../../config';

@Component({
  selector: 'eventos-page',
  templateUrl: 'eventos.html'
})
export class EventosPage {
  segmento: string;
  eventos: any;

  constructor(
    private ga: GoogleAnalytics,
    private nativeStorage: NativeStorage,
    private nav: NavController,
    private eventosProvider: EventosProvider,
    private ux: UxProvider,
  ) {
    this.segmento = 'hoy';

    // Verificar sesión del dispositivo
    // this.events.publish('verificarSesion');

    this.nativeStorage.getItem('token')
      .then(token => {
        this.ux.showCargando();
        this.eventosProvider.getEventos(token)
          .subscribe(dataEventos => {
            this.ux.hideCargando();
            console.log(dataEventos);
            if(dataEventos.status == 'ERROR') {
              this.ux.alerta(dataEventos.error.message);
            } else if(dataEventos.status == 'OK') {
              this.eventos = dataEventos.data;
            }
          }, err => {
            this.ux.hideCargando();
            this.ux.alerta(GlobalVar.SIN_CONEXION);
          });
      }, err => {
        console.log('Error al obtener \'token\': ', err);
      });
  }

  ionViewDidLoad() {
    this.ga.trackView('page-eventos');
  }

  abrirEvento(evento: any) {
    this.nav.push(EventosInfoPage, { evento: evento });
  }
}