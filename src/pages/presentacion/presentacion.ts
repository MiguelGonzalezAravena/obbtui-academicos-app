import { Component, ViewChild } from '@angular/core';
import { NavController, Platform, Slides } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { AccesoPage } from '../acceso/acceso';

@Component({
  selector: 'presentacion-page',
  templateUrl: 'presentacion.html'
})
export class PresentacionPage {
  loading: any;
  lastSlide = false;
  @ViewChild('slider') slider: Slides;

  constructor(
    private platform: Platform,
    private nav: NavController,
    private nativeStorage: NativeStorage
  ) {
  }

  skipIntro() {
    // You can skip to main app
    // this.nav.setRoot(TabsNavigationPage);

    // Or you can skip to last slide (login/signup slide)
    this.lastSlide = true;
    this.slider.slideTo(this.slider.length());
  }

  onSlideChanged() {
    // If it's the last slide, then hide the 'Skip' button on the header
    this.lastSlide = this.slider.isEnd();
  }

  acceder() {
    this.platform.ready()
      .then(() => {
        this.nativeStorage.setItem('presentacion', 'false')
          .then(() => {
            this.nav.setRoot(AccesoPage);
            this.nav.popToRoot();
          }, err => {
            console.error('Error al ir a página acceder: ' + err);
          });
        // localStorage.setItem('presentacion', 'false');
      });
  }
}