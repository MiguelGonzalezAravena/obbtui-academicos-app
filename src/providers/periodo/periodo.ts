import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVar } from '../../config';

@Injectable()
export class PeriodoProvider {
  url: string;

  constructor(private http: HttpClient) {
    this.url = GlobalVar.BASE_API_URL + 'estudiante/periodo';
  }

  getPeriodo(token: string): Observable<any> {
    let headers = { headers: new HttpHeaders().set('Authorization', token) };
    return this.http
      .post(this.url, null, headers)
      .map(function(response) { return response })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
}
